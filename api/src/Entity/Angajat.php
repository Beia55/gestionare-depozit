<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AngajatRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: AngajatRepository::class)]
#[ApiResource(
    itemOperations:["delete", "put" => ["normalization_context" => ["groups" => "read", "write"]]],
    collectionOperations: ["post" => ["normalization_context" => ["groups" => "read", "write"]]]
)]
class Angajat
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\OneToMany(mappedBy: 'idAngajat', targetEntity: Depozit::class, orphanRemoval: true)]
    private $depozits;

    #[ORM\Column(type: 'string', length: 255)]
    private $nume;

    #[ORM\Column(type: 'string', length: 255)]
    private $prenume;

    public function __construct()
    {
        $this->depozits = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Depozit>
     */
    public function getDepozits(): Collection
    {
        return $this->depozits;
    }

    public function addDepozit(Depozit $depozit): self
    {
        if (!$this->depozits->contains($depozit)) {
            $this->depozits[] = $depozit;
            $depozit->setIdAngajat($this);
        }

        return $this;
    }

    public function removeDepozit(Depozit $depozit): self
    {
        if ($this->depozits->removeElement($depozit)) {
            // set the owning side to null (unless already changed)
            if ($depozit->getIdAngajat() === $this) {
                $depozit->setIdAngajat(null);
            }
        }

        return $this;
    }

    public function getNume(): ?string
    {
        return $this->nume;
    }

    public function setNume(string $nume): self
    {
        $this->nume = $nume;

        return $this;
    }

    public function getPrenume(): ?string
    {
        return $this->prenume;
    }

    public function setPrenume(string $prenume): self
    {
        $this->prenume = $prenume;

        return $this;
    }
}
