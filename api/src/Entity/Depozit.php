<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\DepozitRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: DepozitRepository::class)]
#[ApiResource(
    normalizationContext: ["groups" => "read"],
    itemOperations:["get", "put"],
    collectionOperations: ["post" => ["denormalization_context" => ["groups" => "write"]]]
)]
class Depozit
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups("read", "write")]
    private $nume;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups("write")]
    private $locatie;

    #[ORM\Column(type: 'datetime')]
    #[Groups("read", "write")]
    private $dataIntrare;

    #[ORM\Column(type: 'datetime')]
    #[Groups("read")]
    private $dataIesire;

    #[ORM\OneToMany(mappedBy: 'depozit', targetEntity: Marfa::class)]
    #[Groups("read", "write")]
    private $idMarfa;

    #[ORM\ManyToOne(targetEntity: Angajat::class, inversedBy: 'depozits')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups("read", "write")]
    private $idAngajat;

    public function __construct()
    {
        $this->idMarfa = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNume(): ?string
    {
        return $this->nume;
    }

    public function setNume(string $nume): self
    {
        $this->nume = $nume;

        return $this;
    }

    public function getLocatie(): ?string
    {
        return $this->locatie;
    }

    public function setLocatie(string $locatie): self
    {
        $this->locatie = $locatie;

        return $this;
    }

    public function getDataIntrare(): ?\DateTimeInterface
    {
        return $this->dataIntrare;
    }

    public function setDataIntrare(\DateTimeInterface $dataIntrare): self
    {
        $this->dataIntrare = $dataIntrare;

        return $this;
    }

    public function getDataIesire(): ?\DateTimeInterface
    {
        return $this->dataIesire;
    }

    public function setDataIesire(\DateTimeInterface $dataIesire): self
    {
        $this->dataIesire = $dataIesire;

        return $this;
    }

    /**
     * @return Collection<int, Marfa>
     */
    public function getIdMarfa(): Collection
    {
        return $this->idMarfa;
    }

    public function addIdMarfa(Marfa $idMarfa): self
    {
        if (!$this->idMarfa->contains($idMarfa)) {
            $this->idMarfa[] = $idMarfa;
            $idMarfa->setDepozit($this);
        }

        return $this;
    }

    public function removeIdMarfa(Marfa $idMarfa): self
    {
        if ($this->idMarfa->removeElement($idMarfa)) {
            // set the owning side to null (unless already changed)
            if ($idMarfa->getDepozit() === $this) {
                $idMarfa->setDepozit(null);
            }
        }

        return $this;
    }

    public function getIdAngajat(): ?Angajat
    {
        return $this->idAngajat;
    }

    public function setIdAngajat(?Angajat $idAngajat): self
    {
        $this->idAngajat = $idAngajat;

        return $this;
    }
}
